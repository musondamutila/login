package Library;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import net.proteanit.sql.DbUtils;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BookSearch {

	private JFrame frmCentralLibraryManagement;
	private JTextField txtsearch;
	private JTable table;
	private JComboBox comboBoxselect;
	Connection connection=null;
	Statement statement = null;
	ResultSet resultset=null;
	private JLabel lblSearchForBook;
	private JLabel label;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BookSearch window = new BookSearch();
					window.frmCentralLibraryManagement.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public BookSearch() {
		initialize();
		connection = JavaconnectDb.ConnecrDb();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCentralLibraryManagement = new JFrame();
		frmCentralLibraryManagement.setTitle("Central Library Management System");
		frmCentralLibraryManagement.setBounds(100, 100, 701, 305);
		frmCentralLibraryManagement.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmCentralLibraryManagement.getContentPane().setLayout(null);
		
		JLabel lblBook = new JLabel("Search for Book By:");
		lblBook.setFont(new Font("Arial", Font.BOLD, 15));
		lblBook.setForeground(Color.BLACK);
		lblBook.setBounds(48, 80, 172, 19);
		frmCentralLibraryManagement.getContentPane().add(lblBook);
		
		txtsearch = new JTextField();
		txtsearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				try {
					String select=(String)comboBoxselect.getSelectedItem();
					String query="select book_code,Book_Title,author,publisher FROM book WHERE "+select+"=?";
					System.out.println(query);// to show the error
					PreparedStatement pst=connection.prepareStatement(query);
					pst.setString(1, txtsearch.getText());
					ResultSet rs=pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
					//while(rs.next()) {
						
				//	}
				}
				catch(Exception e) {
					e.printStackTrace();
					//JOptionPane.showMessageDialog(null, "Student Record Not Present");
				}
			}
		});
		txtsearch.setBounds(434, 79, 116, 22);
		frmCentralLibraryManagement.getContentPane().add(txtsearch);
		txtsearch.setColumns(10);
		
		comboBoxselect = new JComboBox();
		comboBoxselect.setForeground(Color.BLACK);
		comboBoxselect.setFont(new Font("Arial", Font.PLAIN, 15));
		comboBoxselect.setModel(new DefaultComboBoxModel(new String[] {"Book_Code", "Book_Title"}));
		comboBoxselect.setBounds(230, 79, 172, 22);
		frmCentralLibraryManagement.getContentPane().add(comboBoxselect);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(61, 127, 560, 111);
		frmCentralLibraryManagement.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		lblSearchForBook = new JLabel("Search for Book");
		lblSearchForBook.setForeground(Color.BLACK);
		lblSearchForBook.setFont(new Font("Arial", Font.BOLD, 18));
		lblSearchForBook.setBounds(243, 11, 212, 16);
		frmCentralLibraryManagement.getContentPane().add(lblSearchForBook);
		
		label = new JLabel("");
		label.setBounds(0, 0, 685, 267);
		Image searchbook = new ImageIcon(this.getClass().getResource("/searchbook.jpg")).getImage();
	    Image sea=searchbook.getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_SMOOTH);
	    label.setIcon(new ImageIcon(sea));
		frmCentralLibraryManagement.getContentPane().add(label);
	}

}
