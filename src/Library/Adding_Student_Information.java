
///you idiot
package Library;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class Adding_Student_Information {
	Connection connection=null;
	Statement statement = null;
	ResultSet resultset=null;

	JFrame frmAddigstudent;
	private JTextField txtid;
	private JTextField txtfname;
	private JTextField txtemail;
	private JTextField txtcontact;
	private JTextField txthomeaddress;
	private JComboBox cbogender;
    private JComboBox cbobranch;
    private JTextField txtbranch;
    private JTextField txtgender;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Adding_Student_Information window = new Adding_Student_Information();
					window.frmAddigstudent.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Adding_Student_Information() {
		initialize();
		connection = JavaconnectDb.ConnecrDb();
		frmAddigstudent.setLocationRelativeTo(null);
		
		frmAddigstudent.setVisible(true);

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAddigstudent = new JFrame();
		frmAddigstudent.setTitle("Add_Student");
		frmAddigstudent.setBounds(100, 100, 632, 442);
		frmAddigstudent.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAddigstudent.getContentPane().setLayout(null);
		
		JLabel lblStudentinformation = new JLabel("   Student Information");
		lblStudentinformation.setForeground(Color.BLUE);
		lblStudentinformation.setFont(new Font("Arial", Font.BOLD, 18));
		lblStudentinformation.setBounds(220, 33, 233, 22);
		frmAddigstudent.getContentPane().add(lblStudentinformation);
		
		JLabel lblStudentID = new JLabel("Student ID");
		lblStudentID.setForeground(Color.BLACK);
		lblStudentID.setFont(new Font("Arial", Font.PLAIN, 17));
		lblStudentID.setBounds(28, 94, 100, 16);
		frmAddigstudent.getContentPane().add(lblStudentID);
		
		JLabel lblFristName = new JLabel("Full Name ");
		lblFristName.setBackground(Color.WHITE);
		lblFristName.setForeground(Color.BLACK);
		lblFristName.setFont(new Font("Arial", Font.PLAIN, 16));
		lblFristName.setBounds(28, 123, 100, 16);
		frmAddigstudent.getContentPane().add(lblFristName);
		
		JLabel lblCourse = new JLabel("Course");
		lblCourse.setForeground(Color.BLACK);
		lblCourse.setFont(new Font("Arial", Font.PLAIN, 16));
		lblCourse.setBounds(28, 164, 87, 16);
		frmAddigstudent.getContentPane().add(lblCourse);
		
		JLabel lblEmailAddress = new JLabel("Email Address");
		lblEmailAddress.setForeground(Color.BLACK);
		lblEmailAddress.setFont(new Font("Arial", Font.PLAIN, 16));
		lblEmailAddress.setBounds(28, 207, 109, 16);
		frmAddigstudent.getContentPane().add(lblEmailAddress);
		
		JLabel lblHomeAddress = new JLabel("Home Address");
		lblHomeAddress.setFont(new Font("Arial", Font.PLAIN, 16));
		lblHomeAddress.setBounds(28, 307, 143, 16);
		frmAddigstudent.getContentPane().add(lblHomeAddress);
		
		txtid = new JTextField();
		txtid.setBounds(146, 92, 192, 22);
		frmAddigstudent.getContentPane().add(txtid);
		txtid.setColumns(10);
		
		txtfname = new JTextField();
		txtfname.setBounds(146, 121, 192, 22);
		frmAddigstudent.getContentPane().add(txtfname);
		txtfname.setColumns(10);
		
		txtemail = new JTextField();
		txtemail.setBounds(146, 205, 192, 22);
		frmAddigstudent.getContentPane().add(txtemail);
		txtemail.setColumns(10);
		
		txtcontact = new JTextField();
		txtcontact.setBounds(146, 272, 192, 22);
		frmAddigstudent.getContentPane().add(txtcontact);
		txtcontact.setColumns(10);
		
		txthomeaddress = new JTextField();
		txthomeaddress.setBounds(146, 305, 192, 22);
		frmAddigstudent.getContentPane().add(txthomeaddress);
		txthomeaddress.setColumns(10);
		
		JLabel lblGender = new JLabel("Gender");
		lblGender.setForeground(Color.BLACK);
		lblGender.setFont(new Font("Arial", Font.PLAIN, 16));
		lblGender.setBounds(28, 249, 87, 16);
		frmAddigstudent.getContentPane().add(lblGender);
		
		JLabel lblMobleNumber = new JLabel("Moble Number ");
		lblMobleNumber.setFont(new Font("Arial", Font.PLAIN, 16));
		lblMobleNumber.setBounds(28, 278, 143, 16);
		frmAddigstudent.getContentPane().add(lblMobleNumber);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(txtid.getText().isEmpty() || txtfname.getText().isEmpty() || txtbranch.getText().isEmpty() || txtemail.getText().isEmpty() || txtgender.getText().isEmpty()  || txtcontact.getText().isEmpty() || txthomeaddress.getText().isEmpty() )
				{
					JOptionPane.showMessageDialog(null, "Please Enter all Fields"+"!");
				}
				else{
				try 
				{
					String query ="insert into student (ID,NAME,BRANCH,EMAIL,GENDER,CONTACT,HOME_ADDRESS)values(?,?,?,?,?,?,?)";
					PreparedStatement pst = connection.prepareStatement(query);
					pst.setString(1, txtid.getText());
					pst.setString(2, txtfname.getText());
					pst.setString(3, txtbranch.getText());
					pst.setString(4, txtemail.getText());
					pst.setString(5, txtgender.getText());
					pst.setString(6, txtcontact.getText());
					pst.setString(7, txthomeaddress.getText());
					pst.execute();
					JOptionPane.showMessageDialog(null, "Student Records Saved");
					pst.close();
					txtid.setText("");
					txtfname.setText("");
					txtgender.setText("");
					txtemail.setText("");
					txtgender.setText("");
					txtbranch.setText("");
					txtcontact.setText("");
					txthomeaddress.setText("");
					frmAddigstudent.dispose();
				}
				catch(Exception e) 
				{
					
					JOptionPane.showMessageDialog(null, "Student Record already exits");
					
				}
			
			}}
		});
		btnSave.setForeground(Color.BLUE);
		btnSave.setFont(new Font("Arial", Font.BOLD, 18));
		btnSave.setBackground(Color.WHITE);
		btnSave.setBounds(350, 303, 97, 25);
		frmAddigstudent.getContentPane().add(btnSave);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 17));
		lblNewLabel.setForeground(new Color(0, 0, 0));
		lblNewLabel.setBounds(0, 0, 613, 391);
		 Image sinfor = new ImageIcon(this.getClass().getResource("/s.jpg")).getImage();
		 Image stin=sinfor.getScaledInstance(lblNewLabel.getWidth(), lblNewLabel.getHeight(), Image.SCALE_SMOOTH);
		 lblNewLabel.setIcon(new ImageIcon(stin));
		
		frmAddigstudent.getContentPane().add(lblNewLabel);
		
		txtbranch = new JTextField();
		txtbranch.setForeground(Color.BLACK);
		txtbranch.setBounds(146, 162, 192, 22);
		frmAddigstudent.getContentPane().add(txtbranch);
		txtbranch.setColumns(10);
		
		txtgender = new JTextField();
		txtgender.setBounds(146, 240, 192, 22);
		frmAddigstudent.getContentPane().add(txtgender);
		txtgender.setColumns(10);
	}
}
