package Library;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

public class ISSUE extends JFrame {

	private JPanel contentPane;
	private JFrame ISSUE;
	private JTextField STDID;
	private JTextField STDNAME;
	private JTextField BOID;
	private JTextField BONAME;
	private JTextField DOI;
	private JTextField DOR;
	
	//**********************database related variable declaration*****************
	Connection conn = null;
	Statement stmt = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	private JLabel label;
	private JLabel lblIssuing;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ISSUE frame = new ISSUE();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ISSUE() {
		setTitle("Central Library Management System");
		conn= JavaconnectDb.ConnecrDb();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 691, 422);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblStudentId = new JLabel("STUDENT ID");
		lblStudentId.setFont(new Font("Arial", Font.BOLD, 16));
		lblStudentId.setHorizontalAlignment(SwingConstants.CENTER);
		lblStudentId.setBounds(79, 75, 102, 14);
		contentPane.add(lblStudentId);
		
		JLabel lblStudentName = new JLabel("STUDENT NAME");
		lblStudentName.setFont(new Font("Arial", Font.BOLD, 15));
		lblStudentName.setHorizontalAlignment(SwingConstants.CENTER);
		lblStudentName.setBounds(79, 100, 136, 14);
		contentPane.add(lblStudentName);
		
		JLabel lblBookId = new JLabel("BOOK ID");
		lblBookId.setFont(new Font("Arial", Font.BOLD, 15));
		lblBookId.setHorizontalAlignment(SwingConstants.CENTER);
		lblBookId.setBounds(89, 125, 114, 14);
		contentPane.add(lblBookId);
		
		JLabel lblNewLabel = new JLabel("BOOK NAME");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel.setForeground(new Color(0, 0, 0));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(79, 150, 124, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblDateOfIssue = new JLabel("DATE OF ISSUE");
		lblDateOfIssue.setFont(new Font("Arial", Font.BOLD, 15));
		lblDateOfIssue.setForeground(Color.BLACK);
		lblDateOfIssue.setHorizontalAlignment(SwingConstants.CENTER);
		lblDateOfIssue.setBounds(79, 175, 124, 14);
		contentPane.add(lblDateOfIssue);
		
		JLabel lblNewLabel_1 = new JLabel("DATE OF RETURN");
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(60, 200, 143, 14);
		contentPane.add(lblNewLabel_1);
		
		STDID = new JTextField();
		STDID.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				char c = arg0.getKeyChar();
				if(!(Character.isDigit(c) || (c ==KeyEvent.VK_BACK_SPACE) || (c ==KeyEvent.VK_DELETE))) {
					Toolkit.getDefaultToolkit().beep();
					arg0.consume();
				}
				
			}
		});
		
		STDID.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				
				if(STDID.getText().equals(""))
				{
					
				}
				else
				{
					try {
						

					
						
						pstmt = conn.prepareStatement("SELECT NAME FROM STUDENT WHERE ID=?");
						
						pstmt.setString(1, STDID.getText());
						ResultSet rs = pstmt.executeQuery();
						
						if(!rs.isBeforeFirst()) {
							JOptionPane.showMessageDialog(null, "This Student id does not exist");
							STDID.setText("");
						}
						else if(rs.next()) {
							String a = rs.getString("NAME");
							STDNAME.setText(a);
						}
						
						
					}
					catch (SQLException e) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "SQL Connection Error");
						System.out.println("SOME SQL EXCEPTION");
						e.printStackTrace();
					}
				
				
			}
				
		}
		});
		STDID.setBounds(225, 72, 132, 20);
		contentPane.add(STDID);
		STDID.setColumns(10);
		
		STDNAME = new JTextField();
		STDNAME.setBounds(225, 97, 132, 20);
		contentPane.add(STDNAME);
		STDNAME.setColumns(10);
		
		BOID = new JTextField();
		BOID.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				
				char c = arg0.getKeyChar();
				if(!(Character.isDigit(c) || (c ==KeyEvent.VK_BACK_SPACE) || (c ==KeyEvent.VK_DELETE))) {
					Toolkit.getDefaultToolkit().beep();
					arg0.consume();
				}
				
			}
		});
		BOID.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				
				if (BOID.getText().equals(""))
				{
					
				}
				else{
				
					try {
					
				
					
					pstmt = conn.prepareStatement("SELECT BOOK_TITLE FROM BOOK WHERE BOOK_CODE=?");
					pstmt.setString(1, BOID.getText());
					ResultSet rs = pstmt.executeQuery();
					
					if(!rs.isBeforeFirst()) {
						JOptionPane.showMessageDialog(null, "This Book id does not exist");
						BOID.setText("");
					}
					else if(rs.next()) {
						String a = rs.getString("BOOK_TITLE");
						BONAME.setText(a);
					}
					
					
					}
					catch (SQLException e) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "SQL Connection Error");
						System.out.println("SOME SQL EXCEPTION");
						e.printStackTrace();
					}
				
				}
			}
		});
	
		BOID.setBounds(225, 122, 132, 20);
		contentPane.add(BOID);
		BOID.setColumns(10);
		
		BONAME = new JTextField();
		BONAME.setBounds(225, 147, 132, 20);
		contentPane.add(BONAME);
		BONAME.setColumns(10);
		
		DOI = new JTextField();
		DOI.setFont(new Font("Tahoma", Font.PLAIN, 15));
		DOI.setEditable(false);
		DOI.setBounds(225, 172, 132, 20);
		contentPane.add(DOI);
		DOI.setColumns(10);
		
		LocalDate date = LocalDate.now();
		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		DOI.setText(date.format(formatters));
		
		DOR = new JTextField();
		DOR.setFont(new Font("Tahoma", Font.PLAIN, 15));
		DOR.setEditable(false);
		DOR.setBounds(225, 197, 132, 20);
		contentPane.add(DOR);
		DOR.setColumns(10);
		//**************************DATE
		LocalDate date1 = date.plusDays(5);
		DOR.setText(date1.format(formatters));
		
		
		JButton btnIssueBook = new JButton("ISSUE BOOK");
		btnIssueBook.setFont(new Font("Arial", Font.BOLD, 15));
		btnIssueBook.setForeground(Color.BLUE);
		btnIssueBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				if (BOID.getText().equals("") || BONAME.getText().equals("") || STDID.getText().equals("") || STDNAME.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null, "Cannot have ");
				}
				else{
				
					try {
					
				
					pstmt = conn.prepareStatement("SELECT CURRENT_COPIES FROM BOOK WHERE BOOK_CODE=?");
					pstmt.setString(1, BOID.getText());
					rs = pstmt.executeQuery();
					
					if(rs.next()) {
						Integer s= rs.getInt("CURRENT_COPIES");
						if(s == 0) {
							JOptionPane.showMessageDialog(null, "No copies available for this book");
							rs.close();
							pstmt.close();
							System.exit(0);
						}
						else {
							
							pstmt = conn.prepareStatement("INSERT INTO BORROWER (B_ID,ID,NAME,BOOK_CODE,BOOK_NAME,DATE_OF_ISSUE,DATE_OF_RETURN) VALUES (inc_one.NEXTVAL,?,?,?,?,SYSDATE,SYSDATE+5)");
							pstmt.setString(1, STDID.getText());
							pstmt.setString(2, STDNAME.getText());
							pstmt.setString(3, BOID.getText());
							pstmt.setString(4, BONAME.getText());
							pstmt.executeUpdate();
							JOptionPane.showMessageDialog(null, "Book Issued Succesfully");
							STDID.setText("");
							STDNAME.setText("");
							BOID.setText("");
							BONAME.setText("");
							
							
						}
					}
				
					
					}
					catch (SQLException e) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "SQL Connection Error");
						System.out.println("SOME SQL EXCEPTION");
						e.printStackTrace();
					}
					
					//***********************************************now update quantity of current books left
					try {
						
						
						pstmt = conn.prepareStatement("UPDATE BOOK SET CURRENT_COPIES = CURRENT_COPIES-1 WHERE BOOK_CODE=?");
						pstmt.setString(1, BOID.getText());
						rs = pstmt.executeQuery();
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				
				}
			
			}
		});
		btnIssueBook.setBounds(430, 171, 132, 23);
		contentPane.add(btnIssueBook);
		
		lblIssuing = new JLabel("ISSUING BOOK");
		lblIssuing.setFont(new Font("Arial", Font.BOLD, 15));
		lblIssuing.setForeground(Color.BLACK);
		lblIssuing.setBounds(213, 41, 124, 20);
		contentPane.add(lblIssuing);
		
		label = new JLabel("");
		label.setBounds(0, 0, 675, 373);
		Image iss = new ImageIcon(this.getClass().getResource("/issustud.jpg")).getImage();
	    Image issstu=iss.getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_SMOOTH);
	    label.setIcon(new ImageIcon(issstu));
		contentPane.add(label);
		
	}
}