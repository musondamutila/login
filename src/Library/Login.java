package Library;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class Login {

	Connection connection=null;
	Statement statement = null;
	ResultSet resultset=null;
	
	private JFrame frmLogin;
	private JTextField txtUsername;
	private JLabel label_1;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
		connection = JavaconnectDb.ConnecrDb();
		frmLogin.setLocationRelativeTo(null);
		
		frmLogin.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLogin = new JFrame();
		frmLogin.setTitle("Login");
		frmLogin.setBounds(100, 100, 573, 345);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Username:");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 18));
		lblNewLabel.setBounds(43, 48, 113, 29);
		frmLogin.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Password:");
		lblNewLabel_1.setForeground(Color.BLUE);
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 18));
		lblNewLabel_1.setBounds(43, 113, 113, 29);
		frmLogin.getContentPane().add(lblNewLabel_1);
		
		txtUsername = new JTextField();
		txtUsername.setBounds(204, 52, 116, 22);
		frmLogin.getContentPane().add(txtUsername);
		txtUsername.setColumns(10);
		
		JLabel label = new JLabel("");
		label.setBounds(264, 196, 56, 16);
		frmLogin.getContentPane().add(label);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				
		
		try{
				String query ="select * from LOGIN where USERNAME=? and PASSWORD=?";
		         PreparedStatement pst=connection.prepareStatement(query);
		         pst.setString(1, txtUsername.getText());
		         pst.setString(2, passwordField.getText());
		         
		         ResultSet rs=pst.executeQuery();
		         int count=0;
		        while(rs.next())
		        {
		        	
		        	count= count + 1;
		        	
		        }
		        if(count==1)
		        {
		        	JOptionPane.showMessageDialog(null, "Welcome");
		        	frmLogin.dispose();//this is to hide the login form
		        	User.main(null);//this shows user form after login 
		        	
		        }
		      
		        else if (count>1)
		        {
		        	JOptionPane.showMessageDialog(null, "Duplicate username and password");

		   
		                }
		        else
		        {
		        	JOptionPane.showMessageDialog(null, "Incorrect Username and password");
		        	txtUsername.setText(null);
		        	passwordField.setText(null);
		        }
		        rs.close();
		        pst.close();
		}
		        		
		        catch(Exception e)
		        {
		           JOptionPane.showMessageDialog(null, e);
		        }	
		}
		
			});
		btnLogin.setFont(new Font("Arial", Font.BOLD, 18));
		btnLogin.setForeground(Color.BLUE);
		btnLogin.setBounds(135, 197, 145, 25);
		Image ok = new ImageIcon(this.getClass().getResource("/accept-icon.png")).getImage();
		btnLogin.setIcon(new ImageIcon(ok));
		frmLogin.getContentPane().add(btnLogin);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmLogin = new  JFrame();
				if(JOptionPane.showConfirmDialog(frmLogin, "Confirm if you want to exit","Login System",
						JOptionPane.YES_NO_OPTION)==JOptionPane.YES_NO_OPTION) {
					System.exit(0);
				}
			}
		});
		btnExit.setFont(new Font("Arial", Font.BOLD, 18));
		btnExit.setForeground(Color.BLUE);
		btnExit.setBounds(292, 197, 123, 25);
		Image exit = new ImageIcon(this.getClass().getResource("/Exit.png")).getImage();
		btnExit.setIcon(new ImageIcon(exit));
		frmLogin.getContentPane().add(btnExit);
		
		label_1 = new JLabel("");
		label_1.setBounds(0, 0, 555, 294);
	    Image loginlogo = new ImageIcon(this.getClass().getResource("/log1.jpg")).getImage();
	    Image log=loginlogo.getScaledInstance(label_1.getWidth(), label_1.getHeight(), Image.SCALE_SMOOTH);
	    label_1.setIcon(new ImageIcon(log));
	
		frmLogin.getContentPane().add(label_1);
		
		passwordField = new JPasswordField();
		passwordField.setBackground(Color.WHITE);
		passwordField.setBounds(204, 117, 116, 22);
		frmLogin.getContentPane().add(passwordField);
	}
}
