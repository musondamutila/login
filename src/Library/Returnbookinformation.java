package Library;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JTextField;
import java.awt.Font;

public class Returnbookinformation {

	private JFrame frame;
	private JTextField textBookID;
	private JTextField textMemberID;
	private JTextField textDATE;
	private JTextField textTotalfineamout;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Returnbookinformation window = new Returnbookinformation();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Returnbookinformation() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("Arial", Font.PLAIN, 14));
		frame.setBounds(100, 100, 646, 441);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblBOOKINFORMATION = new JLabel("BOOK Return  INFORMATION");
		lblBOOKINFORMATION.setForeground(Color.BLUE);
		lblBOOKINFORMATION.setFont(new Font("Arial", Font.BOLD, 16));
		lblBOOKINFORMATION.setBounds(157, 13, 251, 16);
		frame.getContentPane().add(lblBOOKINFORMATION);
		
		JLabel lblReturnBook = new JLabel("Return Book :");
		lblReturnBook.setFont(new Font("Arial", Font.PLAIN, 16));
		lblReturnBook.setBounds(50, 81, 136, 16);
		frame.getContentPane().add(lblReturnBook);
		
		JLabel lblBookID = new JLabel("Book ID  :");
		lblBookID.setFont(new Font("Arial", Font.PLAIN, 14));
		lblBookID.setBounds(50, 110, 76, 16);
		frame.getContentPane().add(lblBookID);
		
		JLabel lblMemberID = new JLabel("Member ID :");
		lblMemberID.setFont(new Font("Arial", Font.PLAIN, 14));
		lblMemberID.setBounds(50, 145, 76, 16);
		frame.getContentPane().add(lblMemberID);
		
		JLabel lblDATE = new JLabel("DATE :");
		lblDATE.setFont(new Font("Arial", Font.PLAIN, 14));
		lblDATE.setBounds(50, 179, 56, 16);
		frame.getContentPane().add(lblDATE);
		
		JLabel lblTotalfineamout = new JLabel("Total fine amout :");
		lblTotalfineamout.setFont(new Font("Arial", Font.PLAIN, 14));
		lblTotalfineamout.setBounds(47, 208, 139, 16);
		frame.getContentPane().add(lblTotalfineamout);
		
		JButton btnReturn = new JButton("Return ");
		btnReturn.setForeground(Color.BLUE);
		btnReturn.setFont(new Font("Arial", Font.BOLD, 14));
		btnReturn.setBounds(322, 234, 97, 25);
		frame.getContentPane().add(btnReturn);
		
		JButton btnNewCancel = new JButton("Cancel");
		btnNewCancel.setFont(new Font("Arial", Font.BOLD, 14));
		btnNewCancel.setForeground(Color.BLUE);
		btnNewCancel.setBounds(322, 272, 97, 25);
		frame.getContentPane().add(btnNewCancel);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(Color.BLACK);
		separator.setForeground(Color.BLACK);
		separator.setBounds(419, 211, -418, -13);
		frame.getContentPane().add(separator);
		
		textBookID = new JTextField();
		textBookID.setBounds(187, 107, 182, 22);
		frame.getContentPane().add(textBookID);
		textBookID.setColumns(10);
		
		textMemberID = new JTextField();
		textMemberID.setBounds(187, 142, 182, 22);
		frame.getContentPane().add(textMemberID);
		textMemberID.setColumns(10);
		
		textDATE = new JTextField();
		textDATE.setBounds(187, 176, 182, 22);
		frame.getContentPane().add(textDATE);
		textDATE.setColumns(10);
		
		textTotalfineamout = new JTextField();
		textTotalfineamout.setBounds(187, 205, 182, 22);
		frame.getContentPane().add(textTotalfineamout);
		textTotalfineamout.setColumns(10);
	}

}
