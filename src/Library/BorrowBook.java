package Library;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;

public class BorrowBook {
	Connection connection=null;
	Statement statement = null;
	ResultSet resultset=null;
	
	private JFrame frame;
	private JTextField textEnterBooKID;
	private JTextField textMemberID;
	private JTextField textCurrentDate;
	private JTextField textReturnDate;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BorrowBook window = new BorrowBook();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
   	 */
	public void Books() {
		initialize();
		connection = JavaconnectDb.ConnecrDb();	
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 534, 340);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnBorrow = new JButton("Borrow");
		btnBorrow.addActionListener(new ActionListener() {
			private Connection connection;

			public void actionPerformed(ActionEvent arg0) {
				try 
				{
					String query ="insert into book (book__code,book_title,book_author,publisher,cateory)values(?,?,?,?,?)";
					connection = null;
					PreparedStatement pst = connection.prepareStatement(query);
					pst.setString(1, textEnterBooKID.getText());
					pst.setString(2, textMemberID.getText());
					pst.setString(3, textCurrentDate.getText());
					pst.setString(4, textReturnDate.getText());
					
					
					pst.execute();
					JOptionPane.showMessageDialog(null, "Book Record Saved");
					pst.close();
					
				}
				catch(Exception e) 
				{
					
					JOptionPane.showMessageDialog(null, "Book Record already exits");
					
				}
			
			}
		});
		btnBorrow.setForeground(Color.BLUE);
		btnBorrow.setFont(new Font("Arial", Font.BOLD, 14));
		btnBorrow.setBounds(373, 209, 97, 25);
		frame.getContentPane().add(btnBorrow);
		
		JButton btnNewButton_1 = new JButton("Return to home");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				User.main(null);
				frame.dispose();
			}
		});
		btnNewButton_1.setForeground(Color.BLUE);
		btnNewButton_1.setFont(new Font("Arial", Font.BOLD, 14));
		btnNewButton_1.setBounds(349, 247, 141, 25);
		frame.getContentPane().add(btnNewButton_1);
		
		JLabel lblBookID = new JLabel("Book ID ;");
		lblBookID.setFont(new Font("Arial", Font.BOLD, 14));
		lblBookID.setBounds(90, 83, 97, 16);
		frame.getContentPane().add(lblBookID);
		
		JLabel lblMemberID = new JLabel("Member ID :");
		lblMemberID.setFont(new Font("Arial", Font.BOLD, 14));
		lblMemberID.setBounds(90, 112, 97, 16);
		frame.getContentPane().add(lblMemberID);
		
		JLabel lblCurrentDate = new JLabel("Current Date :");
		lblCurrentDate.setFont(new Font("Arial", Font.BOLD, 14));
		lblCurrentDate.setBounds(90, 141, 116, 16);
		frame.getContentPane().add(lblCurrentDate);
		
		JLabel lblReturnDate = new JLabel("Return Date :");
		lblReturnDate.setFont(new Font("Arial", Font.BOLD, 14));
		lblReturnDate.setBounds(90, 170, 97, 16);
		frame.getContentPane().add(lblReturnDate);
		
		textEnterBooKID = new JTextField();
		textEnterBooKID.setBounds(218, 80, 155, 22);
		frame.getContentPane().add(textEnterBooKID);
		textEnterBooKID.setColumns(10);
		
		textMemberID = new JTextField();
		textMemberID.setBounds(218, 109, 155, 22);
		frame.getContentPane().add(textMemberID);
		textMemberID.setColumns(10);
		
		textCurrentDate = new JTextField();
		textCurrentDate.setBounds(218, 138, 155, 22);
		frame.getContentPane().add(textCurrentDate);
		textCurrentDate.setColumns(10);
		
		textReturnDate = new JTextField();
		textReturnDate.setBounds(218, 167, 155, 22);
		frame.getContentPane().add(textReturnDate);
		textReturnDate.setColumns(10);
		
		JLabel lblBorrowabook = new JLabel("Borrow a book :");
		lblBorrowabook.setFont(new Font("Arial", Font.BOLD, 14));
		lblBorrowabook.setBounds(90, 54, 130, 16);
		frame.getContentPane().add(lblBorrowabook);
		
		JLabel lblBookInformantion = new JLabel("      Book Informantion");
		lblBookInformantion.setForeground(Color.BLUE);
		lblBookInformantion.setFont(new Font("Arial", Font.BOLD, 18));
		lblBookInformantion.setBounds(218, 13, 210, 16);
		frame.getContentPane().add(lblBookInformantion);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(149, 68, 1, 2);
		frame.getContentPane().add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(206, 68, 274, 2);
		frame.getContentPane().add(separator_1);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(24, 243, 492, 2);
		frame.getContentPane().add(separator_2);
	}
}
