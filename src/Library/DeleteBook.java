package Library;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DeleteBook {

	private JFrame frmCentralLibraryManagement;
	private JTextField txtBookCode;
	private JTextField txtBookTitle;
	private JTextField txtBookAuther;
	private JTextField txtPublisher;
	private JTextField txtCatgory;
	Connection connection=null;
	Statement statement = null;
	ResultSet resultset=null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeleteBook window = new DeleteBook();
					window.frmCentralLibraryManagement.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DeleteBook() {
		initialize();
		connection = JavaconnectDb.ConnecrDb();
		frmCentralLibraryManagement.setLocationRelativeTo(null);
		
		frmCentralLibraryManagement.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCentralLibraryManagement = new JFrame();
		frmCentralLibraryManagement.setTitle("Central Library Management System");
		frmCentralLibraryManagement.setBounds(100, 100, 702, 414);
		frmCentralLibraryManagement.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmCentralLibraryManagement.getContentPane().setLayout(null);
		
		txtBookCode = new JTextField();
		txtBookCode.setBounds(294, 137, 116, 22);
		frmCentralLibraryManagement.getContentPane().add(txtBookCode);
		txtBookCode.setColumns(10);
		
		txtBookTitle = new JTextField();
		txtBookTitle.setBounds(294, 179, 116, 22);
		frmCentralLibraryManagement.getContentPane().add(txtBookTitle);
		txtBookTitle.setColumns(10);
		
		txtBookAuther = new JTextField();
		txtBookAuther.setBounds(532, 137, 154, 22);
		frmCentralLibraryManagement.getContentPane().add(txtBookAuther);
		txtBookAuther.setColumns(10);
		
		txtPublisher = new JTextField();
		txtPublisher.setBounds(532, 179, 154, 22);
		frmCentralLibraryManagement.getContentPane().add(txtPublisher);
		txtPublisher.setColumns(10);
		
		txtCatgory = new JTextField();
		txtCatgory.setBounds(532, 225, 136, 22);
		frmCentralLibraryManagement.getContentPane().add(txtCatgory);
		txtCatgory.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("        Delete Book");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setBounds(287, 38, 136, 22);
		frmCentralLibraryManagement.getContentPane().add(lblNewLabel);
		
		JLabel lblBookCode = new JLabel("Book Code");
		lblBookCode.setForeground(Color.BLACK);
		lblBookCode.setFont(new Font("Arial", Font.BOLD, 15));
		lblBookCode.setBounds(174, 139, 97, 16);
		frmCentralLibraryManagement.getContentPane().add(lblBookCode);
		
		JLabel lblBookTitle = new JLabel("Book Title :");
		lblBookTitle.setFont(new Font("Arial", Font.BOLD, 15));
		lblBookTitle.setBounds(174, 182, 97, 16);
		frmCentralLibraryManagement.getContentPane().add(lblBookTitle);
		
		JLabel lblBookAuther = new JLabel("Book Auther :");
		lblBookAuther.setFont(new Font("Arial", Font.BOLD, 15));
		lblBookAuther.setBounds(435, 140, 97, 16);
		frmCentralLibraryManagement.getContentPane().add(lblBookAuther);
		
		JLabel lblPublisher = new JLabel("Publisher :");
		lblPublisher.setFont(new Font("Arial", Font.BOLD, 15));
		lblPublisher.setBounds(435, 182, 87, 16);
		frmCentralLibraryManagement.getContentPane().add(lblPublisher);
		
		JLabel lblCategory = new JLabel("Category:");
		lblCategory.setFont(new Font("Arial", Font.BOLD, 15));
		lblCategory.setBounds(418, 228, 104, 16);
		frmCentralLibraryManagement.getContentPane().add(lblCategory);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
try {
					
					String query="select Book_code,book_title,author,publisher,category FROM book where book_code=?";
					
					System.out.println(query);
					PreparedStatement pst=connection.prepareStatement(query);
					//System.out.println(query);
					pst.setString(1, txtBookCode.getText());
					ResultSet rs=pst.executeQuery();
				
					if(rs.next()) {
						String BookID=rs.getString("Book_Code");
						txtBookCode.setText(BookID);
						String BookTitle=rs.getString("Book_Title");
						txtBookTitle.setText(BookTitle);
						String AU=rs.getString("author");
						txtBookAuther.setText(AU);
						String PU=rs.getString("PUBLISHER");
						txtPublisher.setText(PU);
						String CA=rs.getString("CATEGORY");
						txtCatgory.setText(CA);
						
						
					}
					else 
					{
						JOptionPane.showMessageDialog(null, "Book Record Not Present");

					}
				}
				catch(Exception e) {
					e.printStackTrace();
					
				}}

				
				
			
		});
		btnSearch.setForeground(Color.BLUE);
		btnSearch.setFont(new Font("Arial", Font.BOLD, 15));
		btnSearch.setBounds(453, 299, 97, 25);
		frmCentralLibraryManagement.getContentPane().add(btnSearch);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				

				try 
				{
				//	if(txtStudentID.getText().isEmpty() || txtFullName.getText().isEmpty() || txtCourseBranch.getText().isEmpty() || txxEmailAddress.getText().isEmpty() || txtGender.getText().isEmpty() || txtMobleNumber.getText().isEmpty() || txtHomeAddress.getText().isEmpty() )
					//{
					//	JOptionPane.showMessageDialog(null, "No Searched Record to Remove"+"!");
					//} 
					
					
					String query ="delete book where Book_Code='"+txtBookCode.getText() +"'";
				    
					PreparedStatement pst = connection.prepareStatement(query);
					ResultSet rs=pst.executeQuery();
				    pst.execute();
				    
					JOptionPane.showMessageDialog(null, "Record deleted");
					pst.close();
					
					
					frmCentralLibraryManagement.dispose();
				}
				
				
				catch(Exception e) 
				{
					
					//JOptionPane.showMessageDialog(null, "No Record to delete");
					
				}
							
			
			}
		});
		btnDelete.setFont(new Font("Arial", Font.BOLD, 15));
		btnDelete.setForeground(Color.BLUE);
		btnDelete.setBounds(326, 299, 97, 25);
		frmCentralLibraryManagement.getContentPane().add(btnDelete);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(0, 0, 686, 376);
		Image delete = new ImageIcon(this.getClass().getResource("/delbook.jpg")).getImage();
	    Image del=delete.getScaledInstance(lblNewLabel_1.getWidth(), lblNewLabel_1.getHeight(), Image.SCALE_SMOOTH);
	    lblNewLabel_1.setIcon(new ImageIcon(del));
		frmCentralLibraryManagement.getContentPane().add(lblNewLabel_1);
	}
}
