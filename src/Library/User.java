package Library;

import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JSeparator;
import org.eclipse.wb.swing.FocusTraversalOnArray;

//import LibraryManagementSystem.JavaconnectDb;

import java.awt.Component;
import java.awt.event.WindowStateListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.WindowEvent;
import java.awt.Dimension;
import javax.swing.JTextField;

public class User {

	private JFrame frmCentralLibraryManagement;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JMenuItem mntmAddStudent;
	private JMenuItem mntmNewMenuItem;
	private JMenuItem mntmNewMenuItem_1;
	private JMenu mnNewMenu_1;
	private JMenuItem mntmNewMenuItem_2;
	private JMenuItem mntmNewMenuItem_3;
	private JMenu mnIssue;
	private JMenuItem mntmBookIssue;
	private JMenu mnReturns;
	private JMenuItem mntmReturn;
	private JMenu mnNewMenu_2;
	private JMenuItem mntmNewMenuItem_4;
	private JMenu mnNewMenu_3;
	private JMenuItem mntmNewMenuItem_5;
	private JMenuItem mntmNewMenuItem_6;
	private JLabel user;
	Connection conn = null;
	Statement stmt = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					User window = new User();
					window.frmCentralLibraryManagement.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public User() {
		initialize();
	
	
		frmCentralLibraryManagement.setExtendedState(JFrame.MAXIMIZED_BOTH);//centering the screen
		frmCentralLibraryManagement.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{menuBar, mnNewMenu, mntmAddStudent, mntmNewMenuItem, mntmNewMenuItem_1, mnNewMenu_1, mntmNewMenuItem_2, mntmNewMenuItem_3, mnIssue, mntmBookIssue, mnReturns, mntmReturn, mnNewMenu_2, mntmNewMenuItem_4, mnNewMenu_3, mntmNewMenuItem_5, mntmNewMenuItem_6}));
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCentralLibraryManagement = new JFrame();
		frmCentralLibraryManagement.getContentPane().setSize(new Dimension(32, 0));
		frmCentralLibraryManagement.getContentPane().setLayout(null);
		
		user = new JLabel("");
		user.setBounds(0, 0, 1370, 705);
		Image userimage = new ImageIcon(this.getClass().getResource("/mmm.jpg")).getImage();
	    Image us=userimage.getScaledInstance(user.getWidth(), user.getHeight(), Image.SCALE_SMOOTH);
	    user.setIcon(new ImageIcon(us));
	
		frmCentralLibraryManagement.getContentPane().add(user);
		frmCentralLibraryManagement.addWindowStateListener(new WindowStateListener() {
			public void windowStateChanged(WindowEvent arg0) {
			}
		});
		frmCentralLibraryManagement.setTitle("CENTRAL LIBRARY MANAGEMENT SYSTEM");
		frmCentralLibraryManagement.setBounds(100, 100, 1586, 1030);
		frmCentralLibraryManagement.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		menuBar = new JMenuBar();
		menuBar.setFont(new Font("Arial", Font.BOLD, 18));
		menuBar.setToolTipText("Books");
		frmCentralLibraryManagement.setJMenuBar(menuBar);
		
		mnNewMenu = new JMenu("Student");
		mnNewMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		Image st = new ImageIcon(this.getClass().getResource("/addst.png")).getImage();//adding image to the menu bar student
		mnNewMenu.setIcon(new ImageIcon(st));//adding image to the menu bar student
		mnNewMenu.setForeground(Color.BLUE);
		mnNewMenu.setFont(new Font("Arial", Font.BOLD, 16));
		menuBar.add(mnNewMenu);
		
		mntmAddStudent = new JMenuItem("Add ");
		Image AD = new ImageIcon(this.getClass().getResource("/addst1.png")).getImage();//adding image to the menu bar student
		mntmAddStudent .setIcon(new ImageIcon(AD));//adding image to the menu bar student
		mntmAddStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Adding_Student_Information.main(null);
				frmCentralLibraryManagement.show();
				
			}
		});
		mntmAddStudent.setFont(new Font("Arial", Font.PLAIN, 16));
		mnNewMenu.add(mntmAddStudent);
		
		mntmNewMenuItem = new JMenuItem("Update ");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				studentrecordupdates.main(null);
				
				
			}
		});
		mntmNewMenuItem.setFont(new Font("Arial", Font.PLAIN, 16));
		Image studup = new ImageIcon(this.getClass().getResource("/Studup.png")).getImage();//adding image to the menu bar student
		mntmNewMenuItem.setIcon(new ImageIcon(studup));//adding image to the menu bar student
		mnNewMenu.add(mntmNewMenuItem);
		Image sch = new ImageIcon(this.getClass().getResource("/search.png")).getImage();
		
		mntmNewMenuItem_1 = new JMenuItem("Delete ");
		Image BLT = new ImageIcon(this.getClass().getResource("/recycle.png")).getImage();//adding image to the menu bar student
		mntmNewMenuItem_1.setIcon(new ImageIcon(BLT));//adding image to the menu bar student
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StudentDelete.main(null);
				frmCentralLibraryManagement.show();
			}
		});
		mntmNewMenuItem_1.setFont(new Font("Arial", Font.PLAIN, 16));
		mnNewMenu.add(mntmNewMenuItem_1);
		
		mnNewMenu_1 = new JMenu("Books");
		Image bk = new ImageIcon(this.getClass().getResource("/books.png")).getImage();//adding image to the menu bar student
		mnNewMenu_1.setIcon(new ImageIcon(bk));//adding image to the menu bar student
		mnNewMenu_1.setForeground(Color.BLUE);
		mnNewMenu_1.setFont(new Font("Arial", Font.BOLD, 15));
		menuBar.add(mnNewMenu_1);
		
		mntmNewMenuItem_2 = new JMenuItem("Add");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Books.main(null);
				//frmCentralLibraryManagement.hide();
			}
		});
		Image bk1 = new ImageIcon(this.getClass().getResource("/addbo.png")).getImage();//adding image to the menu bar student
		mntmNewMenuItem_2 .setIcon(new ImageIcon(bk1));//adding image to the menu bar student
		mntmNewMenuItem_2.setFont(new Font("Arial", Font.PLAIN, 15));
		mnNewMenu_1.add(mntmNewMenuItem_2);
		
		mntmNewMenuItem_3 = new JMenuItem("Delete");
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DeleteBook.main(null);
				frmCentralLibraryManagement.show();
				
			}
		});
		Image de = new ImageIcon(this.getClass().getResource("/bookde.png")).getImage();//adding image to the menu bar student
		mntmNewMenuItem_3.setIcon(new ImageIcon(de));//adding image to the menu bar student
		mntmNewMenuItem_3.setFont(new Font("Arial", Font.PLAIN, 15));
		mnNewMenu_1.add(mntmNewMenuItem_3);
		Image RP = new ImageIcon(this.getClass().getResource("/report.png")).getImage();//adding image to the menu bar student
		Image SC = new ImageIcon(this.getClass().getResource("/search.png")).getImage();//adding image to the menu bar student
		
		
		mnIssue = new JMenu("Issue");
		Image bbi = new ImageIcon(this.getClass().getResource("/bookiss.png")).getImage();//adding image to the menu bar student
		mnIssue.setIcon(new ImageIcon(bbi));//adding image to the menu bar student
		mnIssue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		mnIssue.setForeground(Color.BLUE);
		mnIssue.setFont(new Font("Arial", Font.BOLD, 15));
		menuBar.add(mnIssue);
		
		mntmBookIssue = new JMenuItem("Book Issue");
		mntmBookIssue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ISSUE.main(null);
				frmCentralLibraryManagement.show();
				
			}
		});
		mntmBookIssue.setFont(new Font("Arial", Font.PLAIN, 15));
		Image boo = new ImageIcon(this.getClass().getResource("/boo.png")).getImage();//adding image to the menu bar student
		mntmBookIssue.setIcon(new ImageIcon(boo));
		mnIssue.add(mntmBookIssue);
		Image rvs = new ImageIcon(this.getClass().getResource("/reserve.png")).getImage();
		
		mnReturns = new JMenu("Returns");
		Image ret = new ImageIcon(this.getClass().getResource("/ret.png")).getImage();//adding image to the menu bar student
		mnReturns.setIcon(new ImageIcon(ret));//adding image to the menu bar student
		mnReturns.setForeground(Color.BLUE);
		mnReturns.setFont(new Font("Arial", Font.BOLD, 15));
		menuBar.add(mnReturns);
		
		mntmReturn = new JMenuItem("Return Book");
		mntmReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Returns.main(null);
				frmCentralLibraryManagement.show();
			}
		});
		mntmReturn.setFont(new Font("Arial", Font.PLAIN, 15));
		Image returns = new ImageIcon(this.getClass().getResource("/return.png")).getImage();//adding image to the menu bar student
		mntmReturn.setIcon(new ImageIcon(returns));//adding image to the menu bar student
		mnReturns.add(mntmReturn);
		
		mnNewMenu_2 = new JMenu("Reports");
		mnNewMenu_2.setIcon(new ImageIcon(RP));//adding image to the menu bar student
		mnNewMenu_2.setForeground(Color.BLUE);
		mnNewMenu_2.setFont(new Font("Arial", Font.BOLD, 15));
		menuBar.add(mnNewMenu_2);
		
		mntmNewMenuItem_4 = new JMenuItem("Student Report");
		mntmNewMenuItem_4.setFont(new Font("Arial", Font.PLAIN, 15));
		mnNewMenu_2.add(mntmNewMenuItem_4);
		
		mnNewMenu_3 = new JMenu("Search");
		mnNewMenu_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		mnNewMenu_3.setIcon(new ImageIcon(SC));//adding image to the menu bar student
		mnNewMenu_3.setForeground(Color.BLUE);
		mnNewMenu_3.setFont(new Font("Arial", Font.BOLD, 15));
		menuBar.add(mnNewMenu_3);
		
		mntmNewMenuItem_5 = new JMenuItem("Books");
		Image bok = new ImageIcon(this.getClass().getResource("/books.png")).getImage();//adding image to the menu bar student
		mntmNewMenuItem_5.setIcon(new ImageIcon(bok));//adding image to the menu bar student
		mntmNewMenuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BookSearch.main(null);
				frmCentralLibraryManagement.show();
			}
		});
		mntmNewMenuItem_5.setFont(new Font("Arial", Font.PLAIN, 15));
		mnNewMenu_3.add(mntmNewMenuItem_5);
		
		mntmNewMenuItem_6 = new JMenuItem("Student");
		mntmNewMenuItem_6.setFont(new Font("Arial", Font.PLAIN, 15));
		Image AbD = new ImageIcon(this.getClass().getResource("/addst1.png")).getImage();//adding image to the menu bar student
		mntmNewMenuItem_6 .setIcon(new ImageIcon(AbD));//adding image to the menu bar student
		mntmNewMenuItem_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SearchforStudent.main(null);
				frmCentralLibraryManagement.show();
				
			}
		});
		mnNewMenu_3.add(mntmNewMenuItem_6);
		Image images1 = new ImageIcon(this.getClass().getResource("/images1.jpg")).getImage();
		 Image images3 = new ImageIcon(this.getClass().getResource("/images3.jpg")).getImage();
			Image images4 = new ImageIcon(this.getClass().getResource("/images4.jpg")).getImage();
		 Image images6 = new ImageIcon(this.getClass().getResource("/images6.jpg")).getImage();
		 Image selBlue = new ImageIcon(this.getClass().getResource("/selBlue.jpg")).getImage();
	}
}
