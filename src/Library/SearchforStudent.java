package Library;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;

import javax.swing.JTextField;

import net.proteanit.sql.DbUtils;

import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SearchforStudent {

	private JFrame frmCentralLibraryManagement;
	private JTable table;
	private JTextField txtsearch;
	Connection connection=null;
	Statement statement = null;
	ResultSet resultset=null;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchforStudent window = new SearchforStudent();
					window.frmCentralLibraryManagement.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SearchforStudent() {
		initialize();
		connection = JavaconnectDb.ConnecrDb();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCentralLibraryManagement = new JFrame();
		frmCentralLibraryManagement.setTitle("Central Library Management System");
		frmCentralLibraryManagement.setBounds(100, 100, 704, 411);
		frmCentralLibraryManagement.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmCentralLibraryManagement.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Search for student");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 18));
		lblNewLabel.setBounds(231, 70, 212, 16);
		frmCentralLibraryManagement.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_3 = new JLabel("search for student by:");
		lblNewLabel_3.setFont(new Font("Arial", Font.PLAIN, 15));
		lblNewLabel_3.setBounds(68, 142, 150, 16);
		frmCentralLibraryManagement.getContentPane().add(lblNewLabel_3);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(12, 379, 586, 2);
		frmCentralLibraryManagement.getContentPane().add(separator);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(78, 175, 544, 168);
		frmCentralLibraryManagement.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JComboBox comboBoxselect = new JComboBox();
		comboBoxselect.setModel(new DefaultComboBoxModel(new String[] {"ID", "NAME"}));
		comboBoxselect.setBounds(220, 138, 97, 25);
		frmCentralLibraryManagement.getContentPane().add(comboBoxselect);
		
		txtsearch = new JTextField();
		txtsearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				try {
					String select=(String)comboBoxselect.getSelectedItem();
					String query="select ID,NAME,BRANCH,CONTACT,GENDER FROM STUDENT WHERE "+select+"=?";
					System.out.println(query);// to show the error
					PreparedStatement pst=connection.prepareStatement(query);
					pst.setString(1, txtsearch.getText());
					ResultSet rs=pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
					//while(rs.next()) {
						
				//	}
				}
				catch(Exception e) {
					e.printStackTrace();
					//JOptionPane.showMessageDialog(null, "Student Record Not Present");
				}}
				
			
		});
		txtsearch.setBounds(365, 139, 150, 25);
		frmCentralLibraryManagement.getContentPane().add(txtsearch);
		txtsearch.setColumns(10);
		
		JLabel label = new JLabel("");
		label.setBounds(564, 86, 46, 45);
		Image searchbook = new ImageIcon(this.getClass().getResource("/searchbook.jpg")).getImage();
	    Image sea=searchbook.getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_SMOOTH);
	    label.setIcon(new ImageIcon(sea));
		frmCentralLibraryManagement.getContentPane().add(label);
	}
}
