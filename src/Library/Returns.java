package Library;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.MutableComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

public class Returns extends JFrame {

	private JPanel contentPane;
	private JTextField STDID;
	private JTextField STDNAME;
	private JTextField BONAME;
	private JTextField DOI;
	private JTextField DOR;
	private JTextField FINE_ST;
	
	Connection conn = null;
	Statement stmt = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd/MM/YYYY");
	Integer Fine = 10;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Returns frame = new Returns();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Returns() {
		conn = JavaconnectDb.ConnecrDb();
		setTitle("RETURN");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 701, 335);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Student ID");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(0, 58, 92, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("STUDENT NAME");
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel_1.setBounds(0, 83, 120, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("BOOK ID");
		lblNewLabel_2.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(10, 108, 92, 14);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Book Name");
		lblNewLabel_3.setBackground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel_3.setForeground(Color.BLACK);
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3.setBounds(10, 133, 82, 14);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Date Of Issue");
		lblNewLabel_4.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_4.setBounds(0, 158, 102, 14);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Date Of Return");
		lblNewLabel_5.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel_5.setBounds(10, 183, 110, 14);
		contentPane.add(lblNewLabel_5);
		
		JLabel lblFine = new JLabel("Fine");
		lblFine.setFont(new Font("Arial", Font.BOLD, 15));
		lblFine.setHorizontalAlignment(SwingConstants.CENTER);
		lblFine.setBounds(0, 208, 82, 14);
		contentPane.add(lblFine);
		
		JComboBox BOID = new JComboBox();
		BOID.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				BONAME.setText("");
				if(BOID.getSelectedItem() == null)
				{
					
				}
				else
				{
					try {
						
						
						
						pstmt = conn.prepareStatement("SELECT BOOK_NAME FROM BORROWER WHERE BOOK_CODE=?");
						
						pstmt.setString(1, BOID.getSelectedItem().toString());
					
						ResultSet rs = pstmt.executeQuery();
						
						if(!rs.isBeforeFirst()) {
							JOptionPane.showMessageDialog(null, "This BOOK id does not exist. INFACT THIS ERROR SHOULD NOT COME");
							STDID.setText("");
						}
						else if(rs.next()) {
							String a = rs.getString("BOOK_NAME");
							BONAME.setText(a);
						}
					
						
					}
					catch (SQLException e1) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "SQL Connection Error");
						System.out.println("SOME SQL EXCEPTION");
						e1.printStackTrace();
					}
				
				
			}
				
			}
		});
		BOID.setBounds(147, 106, 160, 20);
		contentPane.add(BOID);
		
		STDID = new JTextField();
		STDID.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				
				char c = arg0.getKeyChar();
				if(!(Character.isDigit(c) || (c ==KeyEvent.VK_BACK_SPACE) || (c ==KeyEvent.VK_DELETE))) {
					Toolkit.getDefaultToolkit().beep();
					arg0.consume();
				}
				
			}
		});
		STDID.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				BONAME.setText("");
				BOID.removeAllItems();
				
				if(STDID.getText().equals(""))
				{
					
				}
				else
				{
					try {
						
					
						pstmt = conn.prepareStatement("SELECT NAME,BOOK_CODE FROM BORROWER WHERE ID=?");
						pstmt.setString(1, STDID.getText());
						ResultSet rs = pstmt.executeQuery();
						
						
						if(!rs.isBeforeFirst()) {
							JOptionPane.showMessageDialog(null, "This Student id does not exist OR HAS NOT ISSUED ANY BOOK");
							STDID.setText("");
							return;
						}
						while(rs.next()) {
							String a = rs.getString("NAME");
							STDNAME.setText(a);
							BOID.addItem(rs.getString("BOOK_CODE"));
							
						}
						
					}
					catch (SQLException e) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "SQL Connection Error");
						System.out.println("SOME SQL EXCEPTION");
						e.printStackTrace();
					}
				
				
			}
				
			}
		});
		STDID.setBounds(147, 56, 160, 20);
		contentPane.add(STDID);
		STDID.setColumns(10);
		
		STDNAME = new JTextField();
		STDNAME.setBounds(147, 81, 160, 20);
		contentPane.add(STDNAME);
		STDNAME.setColumns(10);
		
		BONAME = new JTextField();
		BONAME.setBounds(147, 130, 160, 20);
		contentPane.add(BONAME);
		BONAME.setColumns(10);
		
		DOI = new JTextField();
		DOI.setBounds(147, 155, 160, 20);
		contentPane.add(DOI);
		DOI.setColumns(10);
		
		DOR = new JTextField();
		DOR.setBounds(147, 180, 160, 20);
		contentPane.add(DOR);
		DOR.setColumns(10);
		
		FINE_ST = new JTextField();
		FINE_ST.setBounds(147, 205, 160, 20);
		contentPane.add(FINE_ST);
		FINE_ST.setColumns(10);
		
		JButton calcFine = new JButton("Calculate Fine");
		calcFine.setForeground(Color.BLUE);
		calcFine.setFont(new Font("Tahoma", Font.BOLD, 15));
		calcFine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (BONAME.getText().equals("") || STDID.getText().equals("") || STDNAME.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null, "Cannot have ");
				}
				
				try {
					 
						
					
						pstmt = conn.prepareStatement("SELECT DATE_OF_ISSUE,DATE_OF_RETURN FROM BORROWER WHERE ID = ? AND BOOK_CODE=?");
						pstmt.setString(1, STDID.getText());
						pstmt.setString(2, BOID.getSelectedItem().toString());
						//System.out.println(BOID.toString());
						
						ResultSet rs = pstmt.executeQuery();
						
						if(!rs.isBeforeFirst()) {
							JOptionPane.showMessageDialog(null, "This BOOK id does not exist. INFACT THIS ERROR SHOULD NOT COME");
							STDID.setText("");
						}
						else if(rs.next()) {
							
							java.sql.Date retrieved1 = (java.sql.Date) rs.getDate("DATE_OF_ISSUE");
							LocalDate d1 = retrieved1.toLocalDate();
							DOI.setText(d1.format(formatters));
							
							java.sql.Date retrieved2 = (java.sql.Date) rs.getDate("DATE_OF_RETURN");
							LocalDate d2 = retrieved2.toLocalDate();
							DOR.setText(d2.format(formatters));
							
							LocalDate d3 = LocalDate.now();
							
							Period period = Period.between ( d2 , d3 );
							Integer daysElapsed = period.getDays () * Fine;
							if(daysElapsed < 0) {
								FINE_ST.setText("0");
							}
							else {
								FINE_ST.setText(daysElapsed.toString());
							}
						}
						
						
					}
					catch (SQLException e1) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "SQL Connection Error");
						System.out.println("SOME SQL EXCEPTION");
						e1.printStackTrace();
					}
							
				
			}
		});
		calcFine.setBounds(343, 155, 139, 23);
		contentPane.add(calcFine);
		
		JButton RETURN = new JButton("Return");
		RETURN.setFont(new Font("Tahoma", Font.BOLD, 15));
		RETURN.setForeground(Color.BLUE);
		RETURN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Integer bid=0,boid=0,stdid=0;
				String name=null,bookname=null;
				java.sql.Date retrieved1=null;
				java.sql.Date retrieved2=null;
				
				if (BONAME.getText().equals("") || STDID.getText().equals("") || STDNAME.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null, "Cannot have ");
				}
				else{
				//***********************************************************get the details what we are about to delete
					try {
					
						
						pstmt= conn.prepareStatement("SELECT B_ID, ID, NAME, BOOK_CODE, BOOK_NAME, DATE_OF_ISSUE, DATE_OF_RETURN FROM BORROWER WHERE ID = ? AND BOOK_CODE = ?");
						pstmt.setString(1, STDID.getText());
						pstmt.setString(2, BOID.getSelectedItem().toString());
						rs = pstmt.executeQuery();
						
						
						if (rs.next()) {
							bid=rs.getInt("B_ID");
							stdid=rs.getInt("ID");
							name=rs.getString("NAME");
							boid=rs.getInt("BOOK_CODE");
							bookname=rs.getString("BOOK_NAME");
							retrieved1 = (java.sql.Date) rs.getDate("DATE_OF_ISSUE");
							retrieved2 = (java.sql.Date) rs.getDate("DATE_OF_RETURN");
							STDID.setText("");
							
							STDNAME.setText("");
							BONAME.setText("");
							DOI.setText("");
							DOR.setText("");
							FINE_ST.setText("");
						
							
						}
						
						//
					}
					catch (SQLException e2) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "SQL Connection Error");
						System.out.println("SOME SQL EXCEPTION");
						e2.printStackTrace();
					}
					
					}
						
					
						
					//********************************insert those details in bigtable
					try {
							
							pstmt = conn.prepareStatement("INSERT INTO BIGTABLE (B_ID, ID, NAME, BOOK_CODE, BOOK_NAME, DATE_OF_ISSUE, DATE_OF_RETURN,FINE)"
									+ "VALUES (?,?,?,?,?,?,?,?)");
							pstmt.setInt(1, bid);
							pstmt.setInt(2, stdid);
							pstmt.setString(3, name);
							pstmt.setInt(4, boid);
							pstmt.setString(5, bookname);
							pstmt.setDate(6, retrieved1);
							pstmt.setDate(7, retrieved2);
							pstmt.setInt(8, Integer.parseInt(FINE_ST.getText()));
							pstmt.executeUpdate();
							
							
					}
					catch(Exception e3) {
						e3.printStackTrace();
					}
					
					//**************************************now delete the actual entry
					try {
						
						pstmt = conn.prepareStatement("DELETE FROM BORROWER WHERE ID = ? AND BOOK_CODE = ?");
						pstmt.setString(1, STDID.getText());
						pstmt.setString(2, BOID.getSelectedItem().toString());
						pstmt.executeUpdate();
					
						JOptionPane.showMessageDialog(null, "Record Returned Successfully");
					}
					catch(Exception e3) {
						e3.printStackTrace();
					}
						
					//***********************************************now update quantity of current books left
					
					try {
						
						
						pstmt = conn.prepareStatement("UPDATE BOOK SET CURRENT_COPIES = CURRENT_COPIES+1 WHERE BOOK_CODE=?");
						pstmt.setString(1, BOID.getSelectedItem().toString());
						rs = pstmt.executeQuery();
						
					} catch (Exception e5) {
						e5.printStackTrace();
					}
					
					}
				
			
		});
		RETURN.setBounds(343, 205, 139, 23);
		contentPane.add(RETURN);
		
		JLabel label = new JLabel("");
		label.setBounds(0, 0, 685, 297);
		Image ret = new ImageIcon(this.getClass().getResource("/bookreturn.jpg")).getImage();
	    Image del=ret.getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_SMOOTH);
	    label.setIcon(new ImageIcon(del));
		contentPane.add(label);
		
	}
}
