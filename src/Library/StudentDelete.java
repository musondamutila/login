package Library;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class StudentDelete {

	private JFrame frmCentralLibraryManagement;
	private JTextField txtStudentID;
	private JTextField txtFullName;
	private JTextField txtGender;
	private JTextField txtCourseBranch;
	private JTextField txxEmailAddress;
	private JTextField txtMobleNumber;
	private JTextField txtHomeAddress;
	Connection connection=null;
	Statement statement = null;
	ResultSet resultset=null;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StudentDelete window = new StudentDelete();
					window.frmCentralLibraryManagement.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public StudentDelete() {
		initialize();
		connection = JavaconnectDb.ConnecrDb();
		frmCentralLibraryManagement.setLocationRelativeTo(null);
		
		frmCentralLibraryManagement.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCentralLibraryManagement = new JFrame();
		frmCentralLibraryManagement.setTitle("Central Library Management System");
		frmCentralLibraryManagement.setBounds(100, 100, 697, 422);
		frmCentralLibraryManagement.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmCentralLibraryManagement.getContentPane().setLayout(null);
		
		txtStudentID = new JTextField();
		txtStudentID.setBounds(163, 75, 165, 22);
		frmCentralLibraryManagement.getContentPane().add(txtStudentID);
		txtStudentID.setColumns(10);
		
		txtFullName = new JTextField();
		txtFullName.setBounds(163, 112, 165, 22);
		frmCentralLibraryManagement.getContentPane().add(txtFullName);
		txtFullName.setColumns(10);
		
		txtGender = new JTextField();
		txtGender.setBounds(163, 165, 165, 22);
		frmCentralLibraryManagement.getContentPane().add(txtGender);
		txtGender.setColumns(10);
		
		txtCourseBranch = new JTextField();
		txtCourseBranch.setBounds(530, 98, 137, 22);
		frmCentralLibraryManagement.getContentPane().add(txtCourseBranch);
		txtCourseBranch.setColumns(10);
		
		txxEmailAddress = new JTextField();
		txxEmailAddress.setBounds(530, 145, 137, 22);
		frmCentralLibraryManagement.getContentPane().add(txxEmailAddress);
		txxEmailAddress.setColumns(10);
		
		txtMobleNumber = new JTextField();
		txtMobleNumber.setBounds(163, 214, 165, 22);
		frmCentralLibraryManagement.getContentPane().add(txtMobleNumber);
		txtMobleNumber.setColumns(10);
		
		txtHomeAddress = new JTextField();
		txtHomeAddress.setBounds(530, 190, 137, 22);
		frmCentralLibraryManagement.getContentPane().add(txtHomeAddress);
		txtHomeAddress.setColumns(10);
		
		JLabel lblStudentID = new JLabel("Student ID:");
		lblStudentID.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblStudentID.setForeground(Color.WHITE);
		lblStudentID.setBounds(25, 77, 116, 16);
		frmCentralLibraryManagement.getContentPane().add(lblStudentID);
		
		JLabel lblFullName = new JLabel("Full Name :");
		lblFullName.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblFullName.setForeground(Color.WHITE);
		lblFullName.setBackground(Color.BLACK);
		lblFullName.setBounds(23, 114, 104, 16);
		frmCentralLibraryManagement.getContentPane().add(lblFullName);
		
		JLabel lblGender = new JLabel("Gender:");
		lblGender.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblGender.setForeground(Color.WHITE);
		lblGender.setBounds(25, 167, 104, 16);
		frmCentralLibraryManagement.getContentPane().add(lblGender);
		
		JLabel lblCourseBranch = new JLabel("Course:");
		lblCourseBranch.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblCourseBranch.setForeground(new Color(255, 255, 255));
		lblCourseBranch.setBounds(396, 100, 104, 16);
		frmCentralLibraryManagement.getContentPane().add(lblCourseBranch);
		
		JLabel lblEmailAddress = new JLabel("Email Address:");
		lblEmailAddress.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblEmailAddress.setForeground(Color.WHITE);
		lblEmailAddress.setBounds(396, 148, 116, 16);
		frmCentralLibraryManagement.getContentPane().add(lblEmailAddress);
		
		JLabel lblMobleNumber = new JLabel("Moble Number :");
		lblMobleNumber.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblMobleNumber.setForeground(Color.WHITE);
		lblMobleNumber.setBounds(25, 216, 126, 16);
		frmCentralLibraryManagement.getContentPane().add(lblMobleNumber);
		
		JLabel lblHomeAddress = new JLabel("Home Address:");
		lblHomeAddress.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblHomeAddress.setForeground(Color.WHITE);
		lblHomeAddress.setBounds(396, 192, 116, 16);
		frmCentralLibraryManagement.getContentPane().add(lblHomeAddress);
		
		JLabel lblStudentDelete = new JLabel("    Student Delete");
		lblStudentDelete.setFont(new Font("Arial", Font.BOLD, 18));
		lblStudentDelete.setForeground(Color.BLUE);
		lblStudentDelete.setBounds(221, 13, 165, 31);
		frmCentralLibraryManagement.getContentPane().add(lblStudentDelete);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				

				if(txtStudentID.getText().isEmpty()  )
			{
				JOptionPane.showMessageDialog(null, "Please Enter Student ID to be Searched"+"!");
			}
				else {
				try {
							
								

							
					String query="select ID,NAME,BRANCH,EMAIL,GENDER,CONTACT,HOME_ADDRESS FROM STUDENT where id=?";
					PreparedStatement pst=connection.prepareStatement(query);
					pst.setString(1, txtStudentID.getText());
					ResultSet rs=pst.executeQuery();
				
					if(rs.next()) {
						String ID=rs.getString("ID");
						txtStudentID.setText(ID);
						String nam=rs.getString("NAME");
						txtFullName.setText(nam);
						String BR=rs.getString("BRANCH");
						txtCourseBranch.setText(BR);
						String cn=rs.getString("CONTACT");
						txtMobleNumber.setText(cn);
						String em=rs.getString("EMAIL");
						txxEmailAddress.setText(em);
						String gender=rs.getString("GENDER");
						txtGender.setText(gender);
						String ha=rs.getString("HOME_ADDRESS");
						txtHomeAddress.setText(ha);
						JOptionPane.showMessageDialog(null, "Student Record Present");
					}
					
				}
				catch(Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Student Record not Present");
					
				}
			}}
		});
		btnSearch.setForeground(Color.BLUE);
		btnSearch.setFont(new Font("Arial", Font.BOLD, 15));
		btnSearch.setBounds(221, 279, 97, 25);
		frmCentralLibraryManagement.getContentPane().add(btnSearch);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				
				try 
				{
				
					
					
					String query ="delete student where ID='"+txtStudentID.getText() +"'";
				    
					PreparedStatement pst = connection.prepareStatement(query);
					ResultSet rs=pst.executeQuery();
				    pst.execute();
				    
					JOptionPane.showMessageDialog(null, "Record deleted");
					pst.close();
					
					txtStudentID.setText("");
					txtFullName.setText("");
					txtCourseBranch.setText("");
					txtHomeAddress.setText("");
					txxEmailAddress.setText("");
					txtGender.setText("");
					 txtMobleNumber.setText("");
					 frmCentralLibraryManagement.dispose();
					
				}
				
				
				catch(Exception e) 
				{
					
					JOptionPane.showMessageDialog(null, "No Record to delete");
					
				}
							
			}
		});
		btnDelete.setFont(new Font("Arial", Font.BOLD, 15));
		btnDelete.setForeground(Color.BLUE);
		btnDelete.setBounds(341, 279, 97, 25);
		frmCentralLibraryManagement.getContentPane().add(btnDelete);
		
		JLabel lbldelete = new JLabel("");
		lbldelete.setBounds(0, 0, 679, 371);
		Image delete = new ImageIcon(this.getClass().getResource("/Delete.jpg")).getImage();
	    Image del=delete.getScaledInstance(lbldelete.getWidth(), lbldelete.getHeight(), Image.SCALE_SMOOTH);
	    lbldelete.setIcon(new ImageIcon(del));
		frmCentralLibraryManagement.getContentPane().add(lbldelete);
	}

}
