package Library;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Window;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class Books {

	protected static final Window Frame = null;
	Connection connection=null;
	Statement statement = null;
	ResultSet resultset=null;
	private JFrame frmCentralLibraryManagement;
	private JTextField txtBookCode;
	private JTextField txtBookTitle;
	private JTextField txtBookAuther;
	private JTextField txtPublisher;
	private JTextField txtCategory;
	protected Object rame;
	private JTextField txttotalcopy;
	private JTextField txtcurrentcopy;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Books window = new Books();
					window.frmCentralLibraryManagement.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Books() {
		initialize();
		connection = JavaconnectDb.ConnecrDb();
		frmCentralLibraryManagement.setLocationRelativeTo(null);
		
		frmCentralLibraryManagement.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCentralLibraryManagement = new JFrame();
		frmCentralLibraryManagement.setTitle("Central Library Management System");
		frmCentralLibraryManagement.setBounds(100, 100, 701, 444);
		frmCentralLibraryManagement.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmCentralLibraryManagement.getContentPane().setLayout(null);
		
		txtBookCode = new JTextField();
		txtBookCode.setForeground(Color.BLUE);
		txtBookCode.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtBookCode.setBackground(Color.WHITE);
		txtBookCode.setBounds(217, 99, 232, 22);
		frmCentralLibraryManagement.getContentPane().add(txtBookCode);
		txtBookCode.setColumns(10);
		
		JLabel lbBookInformantion = new JLabel("      Book Informantion");
		lbBookInformantion.setForeground(Color.WHITE);
		lbBookInformantion.setFont(new Font("Arial", Font.BOLD, 18));
		lbBookInformantion.setBounds(184, 13, 232, 16);
		frmCentralLibraryManagement.getContentPane().add(lbBookInformantion);
		
		JLabel lblBookCodeNo = new JLabel("Book Code");
		lblBookCodeNo.setForeground(Color.WHITE);
		lblBookCodeNo.setFont(new Font("Arial", Font.BOLD, 14));
		lblBookCodeNo.setBounds(82, 99, 123, 16);
		frmCentralLibraryManagement.getContentPane().add(lblBookCodeNo);
		
		JLabel lblBookAuther = new JLabel("Book Auther :");
		lblBookAuther.setFont(new Font("Arial", Font.BOLD, 14));
		lblBookAuther.setForeground(Color.WHITE);
		lblBookAuther.setBounds(82, 172, 123, 16);
		frmCentralLibraryManagement.getContentPane().add(lblBookAuther);
		
		JLabel lblBookSubject = new JLabel("Publisher :");
		lblBookSubject.setForeground(Color.WHITE);
		lblBookSubject.setFont(new Font("Arial", Font.BOLD, 14));
		lblBookSubject.setBounds(82, 218, 123, 22);
		frmCentralLibraryManagement.getContentPane().add(lblBookSubject);
		
		JLabel lblEditionNumber = new JLabel("Category:");
		lblEditionNumber.setForeground(Color.WHITE);
		lblEditionNumber.setFont(new Font("Arial", Font.BOLD, 14));
		lblEditionNumber.setBounds(82, 251, 123, 16);
		frmCentralLibraryManagement.getContentPane().add(lblEditionNumber);
		
		JLabel lblBookTitle = new JLabel("Book Title :");
		lblBookTitle.setForeground(Color.WHITE);
		lblBookTitle.setFont(new Font("Arial", Font.BOLD, 14));
		lblBookTitle.setBounds(82, 138, 108, 16);
		frmCentralLibraryManagement.getContentPane().add(lblBookTitle);
		
		txtBookTitle = new JTextField();
		txtBookTitle.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtBookTitle.setBounds(217, 134, 232, 22);
		frmCentralLibraryManagement.getContentPane().add(txtBookTitle);
		txtBookTitle.setColumns(10);
		
		txtBookAuther = new JTextField();
		txtBookAuther.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtBookAuther.setBounds(217, 169, 232, 22);
		frmCentralLibraryManagement.getContentPane().add(txtBookAuther);
		txtBookAuther.setColumns(10);
		
		txtPublisher = new JTextField();
		txtPublisher.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtPublisher.setBounds(217, 218, 232, 22);
		frmCentralLibraryManagement.getContentPane().add(txtPublisher);
		txtPublisher.setColumns(10);
		
		txtCategory = new JTextField();
		txtCategory.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtCategory.setBounds(217, 248, 232, 22);
		frmCentralLibraryManagement.getContentPane().add(txtCategory);
		txtCategory.setColumns(10);
		
		JButton btnInsertInfromation = new JButton("Save Information");
		btnInsertInfromation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(txtBookCode.getText().isEmpty() || txtBookTitle.getText().isEmpty() || txtBookAuther.getText().isEmpty() || txtPublisher.getText().isEmpty() || txtCategory.getText().isEmpty()  || txttotalcopy.getText().isEmpty() )
				{
					JOptionPane.showMessageDialog(null, "Please Enter all Fields"+"!");
				}
				else {
				try 
				{
					String query ="insert into book (book_code,book_title,author,publisher,category,current_copies,total_copies)values(?,?,?,?,?,?,?)";
					PreparedStatement pst = connection.prepareStatement(query);
					pst.setString(1, txtBookCode.getText());
					pst.setString(2, txtBookTitle.getText());
					pst.setString(3, txtBookAuther.getText());
					pst.setString(4, txtPublisher.getText());
					pst.setString(5, txtCategory.getText());
					pst.setString(6, txtcurrentcopy.getText());
					pst.setString(7, txttotalcopy.getText());
					
					pst.execute();
					JOptionPane.showMessageDialog(null, "Book Record Saved");
					pst.close();
					txtBookCode.setText("");
					txtBookTitle.setText("");
					txtBookAuther.setText("");
					txtPublisher.setText("");
					txtCategory.setText("");
					txtcurrentcopy.setText("");
					txttotalcopy.setText("");
					
					frmCentralLibraryManagement.dispose();
				}
				catch(Exception e) 
				{
					
					
					
				}
				}
			}
		});
		btnInsertInfromation.setForeground(Color.BLUE);
		btnInsertInfromation.setFont(new Font("Arial", Font.BOLD, 14));
		btnInsertInfromation.setBounds(476, 217, 160, 25);
		frmCentralLibraryManagement.getContentPane().add(btnInsertInfromation);
		
		JLabel lblCurrentCopy = new JLabel("Total Copy");
		lblCurrentCopy.setBackground(Color.LIGHT_GRAY);
		lblCurrentCopy.setFont(new Font("Arial", Font.BOLD, 15));
		lblCurrentCopy.setForeground(Color.WHITE);
		lblCurrentCopy.setBounds(82, 292, 108, 16);
		frmCentralLibraryManagement.getContentPane().add(lblCurrentCopy);
		
		txttotalcopy = new JTextField();
		txttotalcopy.setBounds(217, 291, 232, 20);
		frmCentralLibraryManagement.getContentPane().add(txttotalcopy);
		txttotalcopy.setColumns(10);
		
		JLabel lblCurrentCopies = new JLabel("Current Copy");
		lblCurrentCopies.setFont(new Font("Arial", Font.BOLD, 15));
		lblCurrentCopies.setForeground(Color.WHITE);
		lblCurrentCopies.setBounds(82, 335, 108, 14);
		frmCentralLibraryManagement.getContentPane().add(lblCurrentCopies);
		
		txtcurrentcopy = new JTextField();
		txtcurrentcopy.setBounds(217, 333, 232, 20);
		frmCentralLibraryManagement.getContentPane().add(txtcurrentcopy);
		txtcurrentcopy.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(0, 0, 685, 406);
		Image index = new ImageIcon(this.getClass().getResource("/bookimg.jpg")).getImage();
		Image del=index.getScaledInstance(lblNewLabel.getWidth(), lblNewLabel.getHeight(), Image.SCALE_SMOOTH);
		lblNewLabel.setIcon(new ImageIcon(del));
		frmCentralLibraryManagement.getContentPane().add(lblNewLabel);
	}
}
