package Library;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Image;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class studentrecordupdates {
	Connection connection=null;
	Statement statement = null;
	ResultSet resultset=null;
	
	private JFrame frmCent;
	private JTextField textStudentID;
	private JTextField txtFullname;
	private JTextField textBranch;
	private JTextField textEmail;
	private JTextField textMobleNO;
	private JTextField textHomeAddress;
	private JTextField textGender;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					studentrecordupdates window = new studentrecordupdates();
					window.frmCent.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public studentrecordupdates() {
		initialize();
		connection = JavaconnectDb.ConnecrDb();
		frmCent.setLocationRelativeTo(null);
		
		frmCent.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCent = new JFrame();
		frmCent.setTitle("Central Library Management System");
		frmCent.setBounds(100, 100, 658, 428);
		frmCent.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmCent.getContentPane().setLayout(null);
		
		JLabel lblStudentRecordUpdate = new JLabel("Student Record");
		lblStudentRecordUpdate.setFont(new Font("Arial", Font.BOLD, 18));
		lblStudentRecordUpdate.setForeground(Color.BLUE);
		lblStudentRecordUpdate.setBounds(249, 37, 146, 16);
		frmCent.getContentPane().add(lblStudentRecordUpdate);
		
		JLabel lblFirstName = new JLabel("Full Name:");
		lblFirstName.setForeground(Color.BLUE);
		lblFirstName.setFont(new Font("Arial", Font.BOLD, 17));
		lblFirstName.setBounds(367, 160, 99, 16);
		frmCent.getContentPane().add(lblFirstName);
		
		JLabel lblGender = new JLabel("Gender:");
		lblGender.setForeground(Color.BLUE);
		lblGender.setFont(new Font("Arial", Font.BOLD, 17));
		lblGender.setBounds(72, 189, 81, 16);
		frmCent.getContentPane().add(lblGender);
		
		JLabel lblCouresBranch = new JLabel("Coures/Branch:");
		lblCouresBranch.setForeground(Color.BLUE);
		lblCouresBranch.setFont(new Font("Arial", Font.BOLD, 17));
		lblCouresBranch.setBounds(367, 189, 136, 16);
		frmCent.getContentPane().add(lblCouresBranch);
		
		JLabel lblEmail = new JLabel(" Email:");
		lblEmail.setForeground(Color.BLUE);
		lblEmail.setFont(new Font("Arial", Font.BOLD, 17));
		lblEmail.setBounds(72, 218, 56, 16);
		frmCent.getContentPane().add(lblEmail);
		
		JLabel lblMobleNO = new JLabel("Mobile NO.:");
		lblMobleNO.setForeground(Color.BLUE);
		lblMobleNO.setFont(new Font("Arial", Font.BOLD, 17));
		lblMobleNO.setBounds(72, 247, 146, 16);
		frmCent.getContentPane().add(lblMobleNO);
		
		JLabel lblHomeAddress = new JLabel(" Home Address:");
		lblHomeAddress.setForeground(Color.BLUE);
		lblHomeAddress.setFont(new Font("Arial", Font.BOLD, 17));
		lblHomeAddress.setBounds(72, 276, 136, 16);
		frmCent.getContentPane().add(lblHomeAddress);
		
		JLabel lblStudentID = new JLabel("Student ID:");
		lblStudentID.setForeground(Color.BLUE);
		lblStudentID.setFont(new Font("Arial", Font.BOLD, 15));
		lblStudentID.setBounds(72, 160, 136, 16);
		frmCent.getContentPane().add(lblStudentID);
		
		textStudentID = new JTextField();
		textStudentID.setBounds(220, 157, 135, 22);
		frmCent.getContentPane().add(textStudentID);
		textStudentID.setColumns(10);
		
		txtFullname = new JTextField();
		txtFullname.setBounds(501, 151, 116, 22);
		frmCent.getContentPane().add(txtFullname);
		txtFullname.setColumns(10);
		
		textBranch = new JTextField();
		textBranch.setBounds(501, 186, 127, 22);
		frmCent.getContentPane().add(textBranch);
		textBranch.setColumns(10);
		
		textEmail = new JTextField();
		textEmail.setBounds(220, 215, 136, 22);
		frmCent.getContentPane().add(textEmail);
		textEmail.setColumns(10);
		
		textMobleNO = new JTextField();
		textMobleNO.setBounds(220, 245, 136, 22);
		frmCent.getContentPane().add(textMobleNO);
		textMobleNO.setColumns(10);
		
		textHomeAddress = new JTextField();
		textHomeAddress.setBounds(220, 273, 136, 22);
		frmCent.getContentPane().add(textHomeAddress);
		textHomeAddress.setColumns(10);
		
		textGender = new JTextField();
		textGender.setForeground(new Color(0, 0, 0));
		textGender.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textGender.setBounds(220, 189, 135, 22);
		frmCent.getContentPane().add(textGender);
		textGender.setColumns(10);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(textStudentID.getText().equals("")  )
				
					
				{
					JOptionPane.showMessageDialog(null, "Please Enter Student Number to be updated"+"!");
				}
				
				else {
			try 
			{
				String query ="Update student set ID='"+textStudentID.getText() +"',NAME='"+txtFullname.getText() +"',BRANCH='"+textBranch.getText() +"',EMAIL='"+textEmail.getText() +"',GENDER='"+	textGender.getText() +"',CONTACT='"+textMobleNO.getText() +"',HOME_ADDRESS='"+textHomeAddress.getText() +"' where  ID='"+ textStudentID.getText()+"'";
			    System.out.println(query);
			    
				PreparedStatement pst = connection.prepareStatement(query);
			
			pst.execute();
				
				pst.close();
				JOptionPane.showMessageDialog( null, "Student Record updated Successfully");
				textStudentID.setText("");
				txtFullname.setText("");
				textBranch.setText("");
				textEmail.setText("");
				textGender.setText("");
				textMobleNO.setText("");
				textHomeAddress.setText("");
			
				frmCent.dispose();
			}
			catch(Exception e) 
			{
				
				JOptionPane.showMessageDialog(null, "Student Record you are trying to update does not exist");
				
			}
				}
		
		}
		});
		btnUpdate.setForeground(Color.BLUE);
		btnUpdate.setFont(new Font("Arial", Font.BOLD, 16));
		btnUpdate.setBounds(475, 289, 97, 25);
		frmCent.getContentPane().add(btnUpdate);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				try {
					
					String query="select ID,NAME,BRANCH,EMAIL,GENDER,CONTACT,HOME_ADDRESS FROM STUDENT where id=?";
					
					System.out.println(query);
					PreparedStatement pst=connection.prepareStatement(query);
					//System.out.println(query);
					pst.setString(1, textStudentID.getText());
					ResultSet rs=pst.executeQuery();
				
					if(rs.next()) {
						String ID=rs.getString("ID");
						textStudentID.setText(ID);
						String NAM=rs.getString("NAME");
						txtFullname.setText(NAM);
						String BR=rs.getString("BRANCH");
						textBranch.setText(BR);
						String EM=rs.getString("EMAIL");
						textEmail.setText(EM);
						String GN=rs.getString("GENDER");
						textGender.setText(GN);
						String CN=rs.getString("CONTACT");
						textMobleNO.setText(CN);
						String HA=rs.getString("HOME_ADDRESS");
						textHomeAddress.setText(HA);
						
						
						
						
						
					}
					else 
					{
						JOptionPane.showMessageDialog(null, "Search for Student");

					}
				}
				catch(Exception e) {
					e.printStackTrace();
					
				}}

				
		
			
		});
		btnSearch.setFont(new Font("Arial", Font.BOLD, 15));
		btnSearch.setForeground(Color.BLUE);
		btnSearch.setBounds(369, 290, 97, 25);
		frmCent.getContentPane().add(btnSearch);
		
		JLabel lblstudentupdate = new JLabel("");
		lblstudentupdate.setBounds(0, 13, 640, 377);
		Image stuup = new ImageIcon(this.getClass().getResource("/update.jpg")).getImage();
		 Image stu=stuup.getScaledInstance(lblstudentupdate.getWidth(), lblstudentupdate.getHeight(), Image.SCALE_SMOOTH);
		 lblstudentupdate.setIcon(new ImageIcon(stu));
		frmCent.getContentPane().add(lblstudentupdate);
	    
  }	
}